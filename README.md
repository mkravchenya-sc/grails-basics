#Grails Tasks


To check tasks 2 and 3, just run the application and go to url [http://localhost:8080/helloworld](http://localhost:8080/helloworld). To check task 4, go to url: [http://localhost:8080/helloworld/user](http://localhost:8080/helloworld/user) or follow the Pokemons link from task 3 page after run the application.

####2) Command Line
Задание: 
- после запуска приложения необходимо вывести сообщение в консоль

####3) Binding Data + i18n
Задание: 
- страница где можно переключить язык, после переключения все сообщения показывать в зависимости от выбранного языка 
- Добавить Input Field на страницу, пользователь вводит число с дробной частью, decimal separator должен браться в зависимости от выбранного языка (DE - ‘,’ EN - ‘.’). Значение посылается на Контроллер, автоматически конвертируется в число с дробной частью (через ‘Data Binding’) и возвращается назад на страницу где это значение и выводится 

####4) Gorm, Controllers, Services
#####Задание 1:
Есть сущность User (Name и Birthday), каждый User собирает Покемонов (еще одна сущность Pokemon с полем Name). У User может быть несколько Покемонов, а может и не быть не одного. Необходимо:
- Создать модель базы на GORM 
- Написать приложение которое выводит список Пользователей и их Покемонов (если они есть) на страницу (с поддержкой pagination)
- Добавить Search Form из 2-х полей, User Name и Pokemon Name, значения в  Search Form можно ввести c ‘\*’ на конце. К примеру ‘Tru*’ ->’Trump’. Введенные значения должны ограничивать список. 
- Использовать Criteria для построения запросов

#####Задание 2 (Sitemesh и Layout):
- Добавить Header/Footer через sitemesh, разобраться как он работает, что такое layouts

#####Задание 3:
- Расширить модель User, добавить туда поле Password и переписать задание Login Page (первая версия написанная на Servlet API) через Grails с использование Filters в Grails, 
  Выводим имя залогинившегося пользователя в Header.
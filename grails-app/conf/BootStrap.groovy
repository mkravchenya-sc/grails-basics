import com.scand.pokemon.Pokemon
import com.scand.pokemon.User

import java.text.SimpleDateFormat

class BootStrap {

    def init = { servletContext ->
        println "Hello world to console"

        Pokemon pikachu = new Pokemon(name: "Pikachu").save()
        Pokemon bulbozaur = new Pokemon(name: "Bulbozaur").save()
        Pokemon ivysaur = new Pokemon(name: "Ivysaur").save()
        Pokemon charizard = new Pokemon(name: "Charizard").save()
        Pokemon charmeleon = new Pokemon(name: "Charmeleon").save()

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy")

        new User(name: "Misha", password: "111111", birthday: sdf.parse("30-09-1991"), pokemons: [pikachu, bulbozaur]).save()
        new User(name: "Stepan", password: "111111", birthday: new Date(), pokemons: [ivysaur]).save()
        new User(name: "Vasya", password: "111111", birthday: new Date()).save()
        new User(name: "Vova", password: "111111", birthday: null, pokemons: [charizard, charmeleon]).save()
    }
    def destroy = {
    }
}

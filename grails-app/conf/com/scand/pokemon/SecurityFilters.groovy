package com.scand.pokemon

class SecurityFilters {

    def filters = {
        loginCheck(controller: '*', action: '*') {
            before = {
                if (!session.user && controllerName != 'login') {
                    redirect(controller: 'login', action: 'index')
                    return false
                } else if (session.user && controllerName == 'login' && actionName != 'logout') {
                    redirect(controller: 'user', action: 'index')
                }
            }
        }
    }
}

package com.scand.pokemon

class User {

    String name
    String password
    Date birthday

    static hasMany = [pokemons : Pokemon]

    static constraints = {
        birthday nullable: true
    }
}

package com.scand.pokemon

import grails.transaction.Transactional

@Transactional
class LoginService {

    User login(String name, String password) {
        User.findByNameAndPassword(name, password)
    }
}

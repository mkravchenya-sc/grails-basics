<%@ page import="org.springframework.web.servlet.support.RequestContextUtils" contentType="text/html;charset=UTF-8" %>
<html>
<head>
	<title><g:layoutTitle default="Pokemon"/></title>
	<link rel="icon" href="${resource(dir: "images", file: "pokemon.jpg")}">
	<r:require module="pokemon"/>
	<r:layoutResources/>
</head>

<body>
	<header id="header">
		<nav class="navbar navbar-dark bg-dark shadow-sm">
			<div class="container d-flex justify-content-between">
				<div class="nav" role="navigation">
					<g:form controller="locale" action="changeLocale">
						<g:select class="custom-select"
								  name="lang"
								  from="${['en', 'ru', 'de']}"
								  value="${RequestContextUtils.getLocale(request)}"
								  onchange="submit()"/>
					</g:form>
				</div>

				<g:pageProperty name="page.header"/>

				<g:if test="${session.user}">
					<div class="btn-group">
						<g:link controller="hello" action="index" class="btn btn-outline-info"><g:message code="helloWorld.button.numberConversion"/></g:link>
						<g:link class="btn btn-outline-info" controller="user" action="index">${message(code: 'user.pokemons')}</g:link>
						<g:form controller="login" action="logout" class="mb-0">
							<g:submitButton name="logout" class="btn btn-outline-danger" value="${message(code: 'default.button.logOut')}"/>
						</g:form>
					</div>
				</g:if>
			</div>
		</nav>
	</header>

	<main role="main" class="container" id="main">
		<g:layoutBody/>
	</main>

	<footer class="footer" id="footer">
		<div class="container">
			<span class="text-muted">Copyright © 2018 Mikhail Kravchenya</span>
		</div>
	</footer>

	<r:layoutResources/>
</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: kravchenya
  Date: 11/26/2018
  Time: 9:39 AM
--%>

<%@ page import="org.springframework.web.servlet.support.RequestContextUtils; com.scand.pokemon.Pokemon; com.scand.pokemon.User" contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <r:require module="table"/>
        <title><g:message code="default.title.users"/></title>
    </head>

    <body>
        <g:applyLayout name="main">
            <content tag="header">
                <g:if test="${session.user != null}">
                    <div class="hello-message">
                        <p>${message(code: 'default.message.hello')}, ${session.user.name}!</p>
                    </div>
                </g:if>
            </content>

            <div class="d-flex justify-content-between m-2">
                <div class="table">
                    <div class="table-row">
                        <div class="table-cell">
                            ${message(code: 'default.display.message')}:
                        </div>
                        <div class="table-cell">
                            <g:form>
                                <g:select class="custom-select"
                                          name="max"
                                          from="${[1, 2, 5, 10]}"
                                          value="${max}"
                                          onchange="submit()"/>
                            </g:form>
                        </div>
                        <div class="table-cell">
                            <g:form>
                                <g:actionSubmit class="btn btn-outline-primary"
                                                value="${message(code: 'default.button.allUsers')}" action="index"/>
                            </g:form>
                        </div>
                    </div>
                </div>

                <div class="table">
                    <div class="table-row">
                        <div class="table-cell">
                            <g:form action="search" class="form-inline mt-2 mt-md-0">
                                <fieldset class="form search-field">
                                    <g:field type="hidden" name="max" value="${max}"/>
                                    <g:field type="hidden" name="offset" value="0"/>
                                    <g:field class="form-control mr-sm-2" type="text" name="userName"
                                             placeholder="${message(code: 'user.username')}"
                                             value="${userName}"/>
                                    <g:field class="form-control mr-sm-2" type="text" name="pokemon"
                                             placeholder="${message(code: 'user.pokemon')}"
                                             value="${pokemon}"/>
                                    <g:submitButton class="btn btn-outline-success my-2 my-sm-0" name="convert"
                                                    value="${message(code: 'default.button.search', default: 'Search')}"/>
                                </fieldset>
                            </g:form>
                        </div>
                    </div>
                </div>
            </div>

            <g:message code="user.message.foundRecords" args="[userCount]"/>

            <table class="table table-hover table-dark">
                <thead>
                <tr>
                    <th class="user-cell"><g:message code="user.name"/></th>
                    <th class="user-cell"><g:message code="user.birthday"/></th>
                    <th><g:message code="user.pokemons"/></th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${users}" var="user">
                    <tr>
                        <td class="user-cell">${user.name}</td>
                        <td class="user-cell">
                            <g:if test="${user.birthday}">
                                ${user.birthday.format('dd-MM-yyyy')}
                            </g:if>
                            <g:else>
                                <g:message code="user.common.messge.no.information"/>
                            </g:else>
                        </td>
                        <td>
                            <g:each in="${user.pokemons}" var="pokemon" status="index">
                                ${pokemon.name}<g:if test="${index != user.pokemons.size() - 1}">,</g:if>
                            </g:each>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>

            <div class="pagination justify-content-center">
                <g:paginate total="${userCount}" max="${max}" params="[userName: userName, pokemon: pokemon]"/>
            </div>
        </g:applyLayout>
    </body>
</html>
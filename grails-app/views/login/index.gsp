<html>
<head>
    <r:require module="signin"/>
    <title>${message(code: 'default.title.login')}</title>
</head>
<body>
<g:applyLayout name="main">
    <div class="text-center">
        <g:form class="form-signin" controller="login" action="login">
            <r:img uri="/images/pokemon.jpg" alt="" width="72" height="72"/>
            <h1 class="h3 mb-3 font-weight-normal">${message(code: 'default.message.signIn')}</h1>
            <g:field class="form-control" type="text" name="name" placeholder="${message(code: 'user.name')}"/>
            <g:field class="form-control" type="password" name="password" placeholder="${message(code: 'user.password')}"/>
            <g:if test="${params.isLoginFail}">
                <div class="text-danger">
                    <g:message code="login.validation.message.error"/>
                </div>
            </g:if>
            <g:submitButton class="btn btn-lg btn-primary btn-block" name="${message(code: 'default.button.signIn')}"/>
        </g:form>
    </div>
</g:applyLayout>
</body>
</html>
<%@ page import="org.springframework.web.servlet.support.RequestContextUtils" %>

<html>
<head>
    <title><g:message code="helloWorld.title" /></title>
</head>
<body>
    <g:applyLayout name="main">
        <content tag="header">
            <div class="hello-message">
                <p><g:message code="helloWorld.message" /></p>
            </div>
        </content>

        <g:form action="convertNumber">
            <div class="input-group mb-3 col-sm-4 mt-3">
                <input type="text" id="name" name="number" class="form-control" placeholder="${message(code: 'helloWorld.inputNumber')}" aria-describedby="button-addon2">
                <div class="input-group-append">
                    <g:submitButton class="btn btn-outline-secondary" id="button-addon2" name="convert" value="${message(code: 'helloWorld.convert', default: 'Convert')}"/>
                </div>
            </div>
        </g:form>

        <p class="font-weight-normal">${message(code: 'helloWorld.originalNumber', default: 'Original number')}: ${params.number}</p>
        <p class="font-weight-normal"><g:message code="helloWorld.convertedNumber"/>: ${convertedNumber}</p>
    </g:applyLayout>
</body>
</html>
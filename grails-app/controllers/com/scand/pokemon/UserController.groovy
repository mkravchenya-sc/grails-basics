package com.scand.pokemon

import grails.gorm.PagedResultList

class UserController {

    def index(int max) {
        max = max ?: 10
        [users: User.list(params), userCount: User.count(), max: max]
    }

    def search(String userName, String pokemon, int max, int offset) {
        List uniqueNames = (List) User.createCriteria().list() {
            projections {
                if (userName) {
                    ilike("name", userName.replaceAll("\\*", "%"))
                }

                if (pokemon) {
                    pokemons {
                        ilike('name', pokemon.replaceAll("\\*", "%"))
                    }
                }

                distinct("name")
            }
        }

        PagedResultList result = (PagedResultList) User.createCriteria().list([max: max, offset: offset]) {
            'in'('name', uniqueNames)
        }

        render(view: 'index',
                model: [
                        users       : result,
                        userCount   : result.totalCount,
                        max         : max,
                        userName    : userName,
                        pokemon     : pokemon
                ])
    }
}

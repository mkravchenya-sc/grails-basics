package com.scand.pokemon

class LocaleController {

    def changeLocale() {
        redirect(uri: request.getHeader('referer'))
    }
}

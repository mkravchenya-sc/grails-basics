package com.scand.pokemon

import org.springframework.web.servlet.support.RequestContextUtils

class LoginController {

    LoginService loginService

    def index() {}

    def login(String name, String password) {
        User user = loginService.login(name, password)
        if (user) {
            session.user = user
            redirect(controller: 'hello', action: 'index')
        } else {
            redirect(action: 'index', params: [isLoginFail: true])
        }
    }

    def logout() {
        Locale locale = RequestContextUtils.getLocale(request)
        session.invalidate()
        RequestContextUtils.getLocaleResolver(request).setLocale(request, response, locale)
        redirect(controller: 'login', action: 'index')
    }
}

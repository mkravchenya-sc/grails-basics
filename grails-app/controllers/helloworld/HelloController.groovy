package helloworld

class HelloController {

    def index() {
    }

    def changeLocale() {
        redirect(uri: request.getHeader('referer'))
    }

    def convertNumber(ConverterCommand command) {
        Double number = command.number
        number ? log.debug("Number successfully converted!") : log.error("Number not converted!")
        
        render(view: 'index', model: [convertedNumber: command.number])
    }
}
